create table piii
(
    regionname   varchar(300) null,
    regionfather varchar(300) null
);

INSERT INTO projc2.piii (regionname, regionfather) VALUES ('America', '');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Norte America', 'America');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Sur America', 'America');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Centro America', 'America');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('El Caribe', 'America');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Europa', '');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Europa del Este', 'Europa');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Centro Europa', 'Europa');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Europa del Oeste', 'Europa');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Asia', '');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Lejano Oriente', 'Asia');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Cercano Oriente', 'Asia');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Africa', '');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Norte Africana', 'Africa');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Centro Africana', 'Africa');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Sur Africana', 'Africa');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Mediterraneo', 'Africa');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Oceania', '');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Pacifico Sur', 'Oceania');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Pacifico', 'Oceania');
INSERT INTO projc2.piii (regionname, regionfather) VALUES ('Pacifico Central', 'Oceania');