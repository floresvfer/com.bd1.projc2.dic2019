import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteInventorComponent } from './delete-inventor.component';

describe('DeleteInventorComponent', () => {
  let component: DeleteInventorComponent;
  let fixture: ComponentFixture<DeleteInventorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteInventorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteInventorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
