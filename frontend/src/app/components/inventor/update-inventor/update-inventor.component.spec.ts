import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateInventorComponent } from './update-inventor.component';

describe('UpdateInventorComponent', () => {
  let component: UpdateInventorComponent;
  let fixture: ComponentFixture<UpdateInventorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateInventorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateInventorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
