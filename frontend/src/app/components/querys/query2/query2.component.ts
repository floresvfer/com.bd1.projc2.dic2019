import {Component, OnInit, ViewChild} from '@angular/core';
import {Query1} from "../../../models/query1";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Query2} from "../../../models/query2";
import {QuerysService} from "../../../services/queryss/querys.service";

@Component({
  selector: 'app-query2',
  templateUrl: './query2.component.html',
  styleUrls: ['./query2.component.scss']
})
export class Query2Component implements OnInit {
  displayedColumns = ['senior', 'junior','researcharea'];
  ELEMENT_DATA: Array<Query2> = [];
  dataSource = new MatTableDataSource<Query2>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private queriesService: QuerysService
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.queriesService.query2()
      .subscribe(results => {
        this.dataSource.data = results;
      })
  }

}
