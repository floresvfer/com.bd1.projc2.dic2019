import {Component, OnInit, ViewChild} from '@angular/core';
import {Query1} from "../../../models/query1";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {QuerysService} from "../../../services/queryss/querys.service";
import {Query11} from "../../../models/query11";

@Component({
  selector: 'app-query11',
  templateUrl: './query11.component.html',
  styleUrls: ['./query11.component.scss']
})
export class Query11Component implements OnInit {

  displayedColumns = ['country1', 'country2'];
  ELEMENT_DATA: Array<Query11> = [];
  dataSource = new MatTableDataSource<Query11>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private queriesService: QuerysService
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.queriesService.query11()
      .subscribe(results => {
        this.dataSource.data = results;
      })
  }
}
