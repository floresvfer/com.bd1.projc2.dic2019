import {Component, OnInit, ViewChild} from '@angular/core';
import {Query1} from "../../../models/query1";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {QuerysService} from "../../../services/queryss/querys.service";
import {Query10} from "../../../models/query10";

@Component({
  selector: 'app-query10',
  templateUrl: './query10.component.html',
  styleUrls: ['./query10.component.scss']
})
export class Query10Component implements OnInit {

  displayedColumns = ['name', 'population'];
  ELEMENT_DATA: Array<Query10> = [];
  dataSource = new MatTableDataSource<Query10>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private queriesService: QuerysService
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.queriesService.query10()
      .subscribe(results => {
        this.dataSource.data = results;
      })
  }
}
