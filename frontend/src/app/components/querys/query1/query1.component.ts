import {Component, OnInit, Query, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Query1} from "../../../models/query1";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {QuerysService} from "../../../services/queryss/querys.service";

@Component({
  selector: 'app-query1',
  templateUrl: './query1.component.html',
  styleUrls: ['./query1.component.scss']
})
export class Query1Component implements OnInit {
  displayedColumns = ['country', 'continent','questions'];
  ELEMENT_DATA: Array<Query1> = [];
  dataSource = new MatTableDataSource<Query1>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private queriesService: QuerysService
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.queriesService.query1()
      .subscribe(results => {
        this.dataSource.data = results;
      })
  }

}
