import {Component, OnInit, ViewChild} from '@angular/core';
import {Query1} from "../../../models/query1";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {QuerysService} from "../../../services/queryss/querys.service";
import {Query8} from "../../../models/query8";

@Component({
  selector: 'app-query8',
  templateUrl: './query8.component.html',
  styleUrls: ['./query8.component.scss']
})
export class Query8Component implements OnInit {
  displayedColumns = ['junior', 'senior','researcharea'];
  ELEMENT_DATA: Array<Query8> = [];
  dataSource = new MatTableDataSource<Query8>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private queriesService: QuerysService
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.queriesService.query8()
      .subscribe(results => {
        this.dataSource.data = results;
      })
  }

}
