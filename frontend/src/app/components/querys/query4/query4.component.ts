import {Component, OnInit, ViewChild} from '@angular/core';
import {Query1} from "../../../models/query1";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {QuerysService} from "../../../services/queryss/querys.service";
import {Query4} from "../../../models/query4";

@Component({
  selector: 'app-query4',
  templateUrl: './query4.component.html',
  styleUrls: ['./query4.component.scss']
})
export class Query4Component implements OnInit {
  displayedColumns = ['country', 'success','failure', 'attempts'];
  ELEMENT_DATA: Array<Query4> = [];
  dataSource = new MatTableDataSource<Query4>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private queriesService: QuerysService
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.queriesService.query4()
      .subscribe(results => {
        this.dataSource.data = results;
      })
  }
}
