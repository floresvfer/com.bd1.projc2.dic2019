import {Component, OnInit, ViewChild} from '@angular/core';
import {Query1} from "../../../models/query1";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {QuerysService} from "../../../services/queryss/querys.service";
import {Query6} from "../../../models/query6";

@Component({
  selector: 'app-query6',
  templateUrl: './query6.component.html',
  styleUrls: ['./query6.component.scss']
})
export class Query6Component implements OnInit {

  displayedColumns = ['name', 'salary','comission', 'totalsalary', 'percent'];
  ELEMENT_DATA: Array<Query6> = [];
  dataSource = new MatTableDataSource<Query6>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private queriesService: QuerysService
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.queriesService.query6()
      .subscribe(results => {
        this.dataSource.data = results;
      })
  }

}
