import {Component, OnInit, ViewChild} from '@angular/core';
import {Query1} from "../../../models/query1";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {QuerysService} from "../../../services/queryss/querys.service";
import {Query12} from "../../../models/query12";

@Component({
  selector: 'app-query12',
  templateUrl: './query12.component.html',
  styleUrls: ['./query12.component.scss']
})
export class Query12Component implements OnInit {

  displayedColumns = ['name', 'salary','comission'];
  ELEMENT_DATA: Array<Query12> = [];
  dataSource = new MatTableDataSource<Query12>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private queriesService: QuerysService
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.queriesService.query12()
      .subscribe(results => {
        this.dataSource.data = results;
      })
  }
}
