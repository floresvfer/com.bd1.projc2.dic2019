import {Component, OnInit, ViewChild} from '@angular/core';
import {Query1} from "../../../models/query1";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {QuerysService} from "../../../services/queryss/querys.service";
import {Query3} from "../../../models/query3";

@Component({
  selector: 'app-query3',
  templateUrl: './query3.component.html',
  styleUrls: ['./query3.component.scss']
})
export class Query3Component implements OnInit {
  displayedColumns = ['professional', 'salary','researcharea', 'salaryavg'];
  ELEMENT_DATA: Array<Query3> = [];
  dataSource = new MatTableDataSource<Query3>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private queriesService: QuerysService
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.queriesService.query3()
      .subscribe(results => {
        this.dataSource.data = results;
      })
  }
}
