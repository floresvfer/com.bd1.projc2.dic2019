import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateInventionComponent } from './update-invention.component';

describe('UpdateInventionComponent', () => {
  let component: UpdateInventionComponent;
  let fixture: ComponentFixture<UpdateInventionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateInventionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateInventionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
