import {Component, OnInit, ViewChild} from '@angular/core';
import {Professional} from "../../models/Professional";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Invention} from "../../models/Invention";
import {InventionsService} from "../../services/inventions/inventions.service";

@Component({
  selector: 'app-inventions',
  templateUrl: './inventions.component.html',
  styleUrls: ['./inventions.component.scss']
})
export class InventionsComponent implements OnInit {
  displayedColumns = ['name', 'yearofinvention', 'countryname',  'update', 'delete'];
  ELEMENT_DATA: Array<Invention> = [];
  dataSource = new MatTableDataSource<Invention>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private inventionsService: InventionsService) { }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.getInventions();
  }

  private getInventions(): void {
    this.inventionsService.getInventions()
      .subscribe(inventions => {
        this.dataSource.data = inventions;
      });
  }

  newInvention(): void{

  }

  deleteInvention(id: string) {
    alert(id);
  }

  updateInvention(id: string) {
    alert(id);
  }
}

