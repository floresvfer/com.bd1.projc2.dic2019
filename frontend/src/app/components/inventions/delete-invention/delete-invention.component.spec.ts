import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteInventionComponent } from './delete-invention.component';

describe('DeleteInventionComponent', () => {
  let component: DeleteInventionComponent;
  let fixture: ComponentFixture<DeleteInventionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteInventionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteInventionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
