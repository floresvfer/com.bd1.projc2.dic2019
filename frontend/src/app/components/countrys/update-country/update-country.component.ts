import {Component, Inject, OnInit} from '@angular/core';
import {Country} from "../../../models/Country";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CountrysService} from "../../../services/countrys/countrys.service";
import {RegionsService} from "../../../services/regions/regions.service";
import {Region} from "../../../models/Region";

@Component({
  selector: 'app-update-country',
  templateUrl: './update-country.component.html',
  styleUrls: ['./update-country.component.scss']
})
export class UpdateCountryComponent implements OnInit {
  country: Country;
  regions: Array<Region>;

  constructor(
    public dialogRef: MatDialogRef<UpdateCountryComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data:{
      country: Country
    },
    private countrysService: CountrysService,
    private regionsService: RegionsService
  ) { }

  ngOnInit() {
    this.country = this.data.country;
    this.regionsService.getRegions()
      .subscribe(regions => {
        this.regions = regions;
      })
  }

  onNoClick(): void{
    this.dialogRef.close(this.country);
  }

  updateCountry(){
    console.log(this.country)
    this.countrysService.updateCountry(this.country).subscribe(
      country => {
        this.dialogRef.close(country);
      }
    )
  }

}
