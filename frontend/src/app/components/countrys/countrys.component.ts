import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Country} from "../../models/Country";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {CountrysService} from "../../services/countrys/countrys.service";
import {MatDialog} from "@angular/material/dialog";
import {DeleteCountryComponent} from "./delete-country/delete-country.component";
import {UpdateCountryComponent} from "./update-country/update-country.component";

@Component({
  selector: 'app-regions',
  templateUrl: './countrys.component.html',
  styleUrls: ['./countrys.component.scss']
})
export class CountrysComponent implements OnInit {
  displayedColumns = ['name', 'regionname','capital', 'population', 'area', 'update', 'delete'];
  ELEMENT_DATA: Array<Country> = [];
  dataSource = new MatTableDataSource<Country>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private countrysService: CountrysService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getCountrys();
  }

  getCountrys(): void{
    this.countrysService.getCountrys()
      .subscribe(regions => {
        this.dataSource.data = regions;
      });
  }

  newCountry(): void{

  }

  deleteCountry(country: Country): void{
    const dialogRef = this.dialog.open(DeleteCountryComponent, {
      width: '400px',
      height: '200px',
      data: {
        country: country
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result.status === '1'){
        this.dataSource.data = this.dataSource.data.filter(country => country.country != result.country);
      }
      console.log('The dialog was closed.'+result.status);
    })
  }

  updateCountry(country: Country): void {
    const dialogRef = this.dialog.open(UpdateCountryComponent, {
      width: '700px',
      height: '550px',
      data: {
        country: country
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      let dat = this.dataSource.data;
      dat = dat.map(co => co.country === result.country ? result : co);
      this.dataSource.data = dat;
    })
  }
}





