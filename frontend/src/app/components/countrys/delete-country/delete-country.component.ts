import {Component, Inject, OnInit} from '@angular/core';
import {Country} from "../../../models/Country";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CountrysService} from "../../../services/countrys/countrys.service";

@Component({
  selector: 'app-delete-country',
  templateUrl: './delete-country.component.html',
  styleUrls: ['./delete-country.component.scss']
})
export class DeleteCountryComponent implements OnInit {
  country: Country;

  constructor(public dialogRef: MatDialogRef<DeleteCountryComponent>,
              @Inject(MAT_DIALOG_DATA)
              public data:{
                country: Country
              },
              private countrysService: CountrysService) {
  }

  ngOnInit() {
    this.country = this.data.country;
  }

  onNoClick(): void {
    this.dialogRef.close(this.country);
  }


  deleteCountry() {
    this.countrysService.deleteCountry(this.country).subscribe(
      country => {
        country.status = 1+'';
        this.dialogRef.close(country);
      }
    );
  }
}
