import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Professional} from "../../models/Professional";
import {GetProfessionalsService} from "../../services/getProfessionals/get-professionals.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'app-professionals',
  templateUrl: './professionals.component.html',
  styleUrls: ['./professionals.component.scss']
})
export class ProfessionalsComponent implements OnInit {
  displayedColumns = ['name', 'contractdate', 'salary', 'comission', 'update', 'delete'];
  ELEMENT_DATA: Array<Professional> = [];
  dataSource = new MatTableDataSource<Professional>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private profesionalsService: GetProfessionalsService) { }

  ngOnInit() {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.getProfessionals();
  }

  getProfessionals(): void{
    this.profesionalsService.getProfessionals()
      .subscribe(professionals => {
        this.dataSource.data = professionals;
      });
  }

  newProfessional(): void{

  }

  deleteProfessional(id: string): void{
    alert(id);
  }

  updateProfessional(id: string): void{
    alert(id);
  }
}
