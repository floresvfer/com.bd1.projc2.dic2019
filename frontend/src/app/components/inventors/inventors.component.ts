import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Inventor} from "../../models/Inventor";
import {InventorsService} from "../../services/inventors/inventors.service";

@Component({
  selector: 'app-inventors',
  templateUrl: './inventors.component.html',
  styleUrls: ['./inventors.component.scss']
})
export class InventorsComponent implements OnInit {
  displayedColumns = ['name', 'country', 'update', 'delete'];
  ELEMENT_DATA: Array<Inventor> = [];
  dataSource = new MatTableDataSource<Inventor>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private inventorsService: InventorsService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getInventors();
  }

  getInventors(): void{
    this.inventorsService.getInventors()
      .subscribe(inventors => {
        this.dataSource.data = inventors;
      });
  }

  deleteInventor(id: string): void {
    alert(id);
  }

  updateInventor(id: string): void {
    alert(id);
  }

}
