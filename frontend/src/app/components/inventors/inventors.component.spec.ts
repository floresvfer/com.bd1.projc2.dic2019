import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventorsComponent } from './inventors.component';

describe('InventorsComponent', () => {
  let component: InventorsComponent;
  let fixture: ComponentFixture<InventorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
