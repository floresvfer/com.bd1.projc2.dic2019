export class Country {
  country: string;
  region: string;
  name: string;
  capital: string;
  population: string;
  area: string;
  status: string;
  regionname: string;
}
