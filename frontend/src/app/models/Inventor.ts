export class Inventor {
  inventor: string;
  country: string;
  name: string;
  status: string;
  countryname: string;
}
