import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import {MatCardModule} from "@angular/material/card";
import { ProfessionalsComponent } from './components/professionals/professionals.component';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {HttpClientModule} from '@angular/common/http';
import {MatSortModule} from "@angular/material/sort";
import { CountrysComponent } from './components/countrys/countrys.component';
import { InventorsComponent } from './components/inventors/inventors.component';
import { InventionsComponent } from './components/inventions/inventions.component';
import { DeleteCountryComponent } from './components/countrys/delete-country/delete-country.component';
import { UpdateCountryComponent } from './components/countrys/update-country/update-country.component';
import {MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import { DeleteProfessionalComponent } from './components/professionals/delete-professional/delete-professional.component';
import { UpdateProfessionalComponent } from './components/professionals/update-professional/update-professional.component';
import { DeleteInventionComponent } from './components/inventions/delete-invention/delete-invention.component';
import { UpdateInventionComponent } from './components/inventions/update-invention/update-invention.component';
import { DeleteInventorComponent } from './components/inventor/delete-inventor/delete-inventor.component';
import { UpdateInventorComponent } from './components/inventor/update-inventor/update-inventor.component';
import {FormsModule} from "@angular/forms";
import {MatSelectModule} from "@angular/material/select";
import { QuerysComponent } from './components/querys/querys.component';
import {MatExpansionModule} from "@angular/material/expansion";
import { Query1Component } from './components/querys/query1/query1.component';
import { Query2Component } from './components/querys/query2/query2.component';
import { Query3Component } from './components/querys/query3/query3.component';
import { Query4Component } from './components/querys/query4/query4.component';
import { Query5Component } from './components/querys/query5/query5.component';
import { Query6Component } from './components/querys/query6/query6.component';
import { Query7Component } from './components/querys/query7/query7.component';
import { Query8Component } from './components/querys/query8/query8.component';
import { Query9Component } from './components/querys/query9/query9.component';
import { Query10Component } from './components/querys/query10/query10.component';
import { Query11Component } from './components/querys/query11/query11.component';
import { Query12Component } from './components/querys/query12/query12.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    HomeComponent,
    ProfessionalsComponent,
    CountrysComponent,
    InventorsComponent,
    InventionsComponent,
    DeleteCountryComponent,
    UpdateCountryComponent,
    DeleteProfessionalComponent,
    UpdateProfessionalComponent,
    DeleteInventionComponent,
    UpdateInventionComponent,
    DeleteInventorComponent,
    UpdateInventorComponent,
    QuerysComponent,
    Query1Component,
    Query2Component,
    Query3Component,
    Query4Component,
    Query5Component,
    Query6Component,
    Query7Component,
    Query8Component,
    Query9Component,
    Query10Component,
    Query11Component,
    Query12Component
  ],
  entryComponents: [
    DeleteCountryComponent,
    UpdateCountryComponent,
    DeleteInventionComponent,
    UpdateInventionComponent,
    DeleteInventorComponent,
    UpdateInventorComponent,
    DeleteProfessionalComponent,
    UpdateProfessionalComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    FormsModule,
    MatSelectModule,
    MatExpansionModule
  ],
  providers: [
    DeleteCountryComponent,
    UpdateCountryComponent,
    DeleteInventionComponent,
    UpdateInventionComponent,
    DeleteInventorComponent,
    UpdateInventorComponent,
    DeleteProfessionalComponent,
    UpdateProfessionalComponent,
    {provide: MatDialogRef, useValue: {}}],
  bootstrap: [AppComponent]
})
export class AppModule { }
