import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./components/login/login.component";
import {ProfessionalsComponent} from "./components/professionals/professionals.component";
import {HomeComponent} from "./components/home/home.component";
import {CountrysComponent} from "./components/countrys/countrys.component";
import {InventorsComponent} from "./components/inventors/inventors.component";
import {InventionsComponent} from "./components/inventions/inventions.component";
import {QuerysComponent} from "./components/querys/querys.component";


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'professionals', component: ProfessionalsComponent},
  {path: 'countrys', component: CountrysComponent},
  {path: 'inventors', component: InventorsComponent},
  {path: 'inventions', component: InventionsComponent},
  {path: 'querys', component: QuerysComponent},
  {path: 'home', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
