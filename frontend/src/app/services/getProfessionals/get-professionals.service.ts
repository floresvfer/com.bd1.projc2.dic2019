import { Injectable } from '@angular/core';
import {Utils} from "../../Utils";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Professional} from "../../models/Professional";

@Injectable({
  providedIn: 'root'
})
export class GetProfessionalsService {
  url = Utils.apiURL;

  constructor(private httpClient: HttpClient) { }

  getProfessionals(): Observable<Professional[]> {
    return this.httpClient.get<Professional[]>(
      this.url + 'getProfessionals.php'
    )
  }
}
