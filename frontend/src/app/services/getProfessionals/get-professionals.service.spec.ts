import { TestBed } from '@angular/core/testing';

import { GetProfessionalsService } from './get-professionals.service';

describe('GetProfessionalsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetProfessionalsService = TestBed.get(GetProfessionalsService);
    expect(service).toBeTruthy();
  });
});
