import { Injectable } from '@angular/core';
import {Utils} from "../../Utils";
import {Observable} from "rxjs";
import {Inventor} from "../../models/Inventor";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class InventorsService {
  url = Utils.apiURL;

  constructor(private httpClient: HttpClient) { }

  getInventors(): Observable<Inventor[]>{
    return this.httpClient.get<Inventor[]>(
      this.url + 'getInventors.php'
    )
  }
}
