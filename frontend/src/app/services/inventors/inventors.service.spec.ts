import { TestBed } from '@angular/core/testing';

import { InventorsService } from './inventors.service';

describe('InventorsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InventorsService = TestBed.get(InventorsService);
    expect(service).toBeTruthy();
  });
});
