import {Injectable} from '@angular/core';
import {Utils} from "../../Utils";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Country} from "../../models/Country";

@Injectable({
  providedIn: 'root'
})
export class CountrysService {
  url = Utils.apiURL;

  constructor(private httpClient: HttpClient) {}

  getCountrys(): Observable<Country[]> {
    return this.httpClient.get<Country[]>(
      this.url + 'getCountrys.php'
    )
  }

  deleteCountry(country: Country): Observable<Country> {
    return this.httpClient.post<Country>(
      this.url + 'deleteCountry.php',
      JSON.stringify({
        country
      })
    );
  }

  updateCountry(country: Country): Observable<Country> {
    return this.httpClient.post<Country>(
      this.url + 'updateCountry.php',
      JSON.stringify({
        country
      })
    )
  };
}
