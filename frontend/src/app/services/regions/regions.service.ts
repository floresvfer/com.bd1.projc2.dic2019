import { Injectable } from '@angular/core';
import {Utils} from "../../Utils";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Region} from "../../models/Region";

@Injectable({
  providedIn: 'root'
})
export class RegionsService {
  url = Utils.apiURL;

  constructor(private httpClient: HttpClient) { }

  getRegions(): Observable<Region[]>{
    return this.httpClient.get<Region[]>(
      this.url + 'getRegions.php'
    )
  }
}
