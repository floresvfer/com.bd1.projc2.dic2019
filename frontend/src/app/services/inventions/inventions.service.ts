import { Injectable } from '@angular/core';
import {Utils} from "../../Utils";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Invention} from "../../models/Invention";

@Injectable({
  providedIn: 'root'
})
export class InventionsService {
  url = Utils.apiURL;

  constructor(private httpClient: HttpClient) { }

  getInventions(): Observable<Invention[]> {
    return this.httpClient.get<Invention[]>(
      this.url + 'getInventions.php'
    )
  }
}
