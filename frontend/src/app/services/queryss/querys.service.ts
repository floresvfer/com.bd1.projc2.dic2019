import { Injectable } from '@angular/core';
import {Utils} from "../../Utils";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Query1} from "../../models/query1";
import {Query2} from "../../models/query2";
import {Query3} from "../../models/query3";
import {Query4} from "../../models/query4";
import {Query5} from "../../models/query5";
import {Query6} from "../../models/query6";
import {Query7} from "../../models/query7";
import {Query8} from "../../models/query8";
import {Query9} from "../../models/query9";
import {Query10} from "../../models/query10";
import {Query11} from "../../models/query11";
import {Query12} from "../../models/query12";

@Injectable({
  providedIn: 'root'
})
export class QuerysService {
  url = Utils.apiURL;

  constructor(private httpClient: HttpClient) { }

  query1(): Observable<Query1[]> {
    return this.httpClient.get<Query1[]>(
      this.url + 'query1.php'
    )
  }

  query2(): Observable<Query2[]> {
    return this.httpClient.get<Query2[]>(
      this.url + 'query2.php'
    )
  }

  query3(): Observable<Query3[]> {
    return this.httpClient.get<Query3[]>(
      this.url + 'query3.php'
    )
  }

  query4(): Observable<Query4[]> {
    return this.httpClient.get<Query4[]>(
      this.url + 'query4.php'
    )
  }

  query5(): Observable<Query5[]> {
    return this.httpClient.get<Query5[]>(
      this.url + 'query5.php'
    )
  }

  query6(): Observable<Query6[]> {
    return this.httpClient.get<Query6[]>(
      this.url + 'query6.php'
    )
  }

  query7(): Observable<Query7[]> {
    return this.httpClient.get<Query7[]>(
      this.url + 'query7.php'
    )
  }

  query8(): Observable<Query8[]> {
    return this.httpClient.get<Query8[]>(
      this.url + 'query8.php'
    )
  }

  query9(): Observable<Query9[]> {
    return this.httpClient.get<Query9[]>(
      this.url + 'query9.php'
    )
  }

  query10(): Observable<Query10[]> {
    return this.httpClient.get<Query10[]>(
      this.url + 'query10.php'
    )
  }

  query11(): Observable<Query11[]> {
    return this.httpClient.get<Query11[]>(
      this.url + 'query11.php'
    )
  }

  query12(): Observable<Query12[]> {
    return this.httpClient.get<Query12[]>(
      this.url + 'query12.php'
    )
  }
}
