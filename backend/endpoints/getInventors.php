<?php
require_once '../lib/header.php';
require_once '../lib/request.php';

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $query = "
        select inventor.*, c.name countryname from inventor join country c on inventor.country = c.country;
        ";

        $request = new request($query);
        echo $request->response();
        break;
}
