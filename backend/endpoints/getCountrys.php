<?php
require_once '../lib/header.php';
require_once '../lib/request.php';

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $query = "
        select country.*, r.name regionname from country join region r on country.region = r.region where country.status = 0;
        ";

        $request = new request($query);
        echo $request->response();
        break;
}
