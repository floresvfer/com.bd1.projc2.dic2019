<?php
require_once '../lib/header.php';
require_once '../lib/request.php';

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $query = "





select
       country.name country,
       sum(case when e.answer = ca.answer then 1 else 0 end) success,
       sum(case when e.answer != ca.answer then 1 else 0 end) failure,
       count(e.question) attempts
from country
         join evaluation e on country.country = e.country
         join question q on e.question = q.question
         join answer a on e.answer = a.answer
         join correct_answer ca on q.question = ca.question
group by country.name
order by success desc
;





        ";

        $request = new request($query);
        echo $request->response();
        break;
}
