<?php
require_once '../lib/header.php';
require_once '../lib/request.php';

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $query = "




select p2.name junior, p.name senior, ra.description researcharea
from researcharea ra
         left join professional p on ra.boss = p.professional
         join professional_researcharea pr on ra.researcharea = pr.researcharea
         join professional p2 on pr.professional = p2.professional
where ra.researcharea not in (
    select r.researcharea
    from professional_invention pi
             join professional p on pi.professional = p.professional
             join invention ion on pi.invention = ion.invention
             join inventor_invention ii on ion.invention = ii.invention
             join inventor ior on ii.inventor = ior.inventor
             join professional_researcharea pr on p.professional = pr.professional
             join researcharea r on pr.researcharea = r.researcharea
    where lower(ior.name) like 'Pasteur'
)
order by junior;









        ";

        $request = new request($query);
        echo $request->response();
        break;
}
