<?php
require_once '../lib/header.php';
require_once '../lib/request.php';

switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        if(empty($_POST))
            $_POST = json_decode(file_get_contents('php://input'), true);
        $country = $_POST['country'];

        deleteCountry($country);
        echo json_encode($country);
        break;
}

function deleteCountry($country){
    $countryId = $country['country'];
    $query =
        "
        update country set status = 1 where country = $countryId;
        ";
    $request = new request($query);
    $request->execute();
}
