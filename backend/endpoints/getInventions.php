<?php
require_once '../lib/header.php';
require_once '../lib/request.php';

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $query = "
        select invention.*, c.name countryname from invention join country c on invention.country = c.country;
        ";

        $request = new request($query);
        echo $request->response();
        break;
}
