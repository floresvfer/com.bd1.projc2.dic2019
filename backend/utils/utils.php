<?php
header('Content-Type: application/json');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once '../controlador.php';
require_once '../lib/Servidor_Base_Datos.php';


function getCicloActivo(){
  $bdd = new Servidor_Base_Datos($_SESSION['DBServer1'], $_SESSION['DBUser1'], $_SESSION['DBPwd1'], $_SESSION['DBName1']);

  $campos = 'CICLO';
  $tabla = 'CICLO';
  $restriccion = "activo = 69";
  $order = '';

  $bdd->cunsultaTradicional($campos, $tabla, $restriccion, $order);
  if($bdd->numero_filas() > 0){
    return $bdd->extraer_registro()['CICLO'];
  }else{
    return 0;
  }
}


function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

function getCicloActivoNombre(){
    $bdd = new Servidor_Base_Datos($_SESSION['DBServer1'], $_SESSION['DBUser1'], $_SESSION['DBPwd1'], $_SESSION['DBName1']);

    $campos = 'descripcion';
    $tabla = 'CICLO';
    $restriccion = "activo = 69";
    $order = '';
    $bdd->cunsultaTradicional($campos, $tabla, $restriccion, $order);
    $rs = '';

    if($bdd->numero_filas() > 0)
        $rs = $bdd->extraer_registro()['descripcion'];

    if(startsWith($rs, "Primer"))
        return 'Primer';
    return 'Segundo';
}