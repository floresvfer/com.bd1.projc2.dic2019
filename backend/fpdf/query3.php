<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('html_table.php');
require_once '../controlador.php';
require_once '../lib/Servidor_Base_Datos.php';
require_once '../lib/toJSON.php';
require_once '../utils/utils.php';
require('../lib/request.php');

header('Content-Type: text/html');
$htmlTable='<table>
<th>
<td></td>
<td>Professional</td>
<td>Salary</td>
<td>Research Area</td>
<td>Salary Average</td>
</th>';

$query = "
select
       p.name professional,
       p.salary salary,
       r.description researcharea,
       avgs.avg salaryavg
from professional p
        join professional_researcharea pr on p.professional = pr.professional
        join researcharea r on r.researcharea = pr.researcharea,
        (select
               avg(p.salary) avg,
               r.researcharea
        from professional p
                join professional_researcharea pr on p.professional = pr.professional
                join researcharea r on r.researcharea = pr.researcharea
                group by r.researcharea) avgs
where
      avgs.researcharea = r.researcharea and
      p.salary > avgs.avg
order by r.description;
";

$request = new request($query);
$array = $request->arrayResponse();

for($i = 0; $i < sizeof($array); $i++){
$htmlTable.= '<tr>';
$htmlTable.= '<td>'.($i+1).'</td>';
$htmlTable.= '<td>'.$array[$i]['professional'].'</td>';
$htmlTable.= '<td>'.$array[$i]['salary'].'</td>';
$htmlTable.= '<td>'.$array[$i]['researcharea'].'</td>';
$htmlTable.= '<td>'.$array[$i]['salaryavg'].'</td>';
$htmlTable.= '</tr>';
}

$htmlTable.= '</table>';

$pdf=new PDF_HTML_Table();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);
$pdf->WriteHTML("<h1>QUERY 3</h1><br>$htmlTable");
$pdf->Output();
?>
