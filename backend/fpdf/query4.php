<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('html_table.php');
require_once '../controlador.php';
require_once '../lib/Servidor_Base_Datos.php';
require_once '../lib/toJSON.php';
require_once '../utils/utils.php';
require('../lib/request.php');

header('Content-Type: text/html');
$htmlTable='<table>
<th>
<td></td>
<td>Country</td>
<td>Success</td>
<td>Failure</td>
<td>Attempts</td>
</th>';

$query = "
select
       country.name country,
       sum(case when e.answer = ca.answer then 1 else 0 end) success,
       sum(case when e.answer != ca.answer then 1 else 0 end) failure,
       count(e.question) attempts
from country
         join evaluation e on country.country = e.country
         join question q on e.question = q.question
         join answer a on e.answer = a.answer
         join correct_answer ca on q.question = ca.question
group by country.name
order by success desc
;

";

$request = new request($query);
$array = $request->arrayResponse();

for($i = 0; $i < sizeof($array); $i++){
$htmlTable.= '<tr>';
$htmlTable.= '<td>'.($i+1).'</td>';
$htmlTable.= '<td>'.$array[$i]['country'].'</td>';
$htmlTable.= '<td>'.$array[$i]['success'].'</td>';
$htmlTable.= '<td>'.$array[$i]['failure'].'</td>';
$htmlTable.= '<td>'.$array[$i]['attempts'].'</td>';
$htmlTable.= '</tr>';
}

$htmlTable.= '</table>';

$pdf=new PDF_HTML_Table();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);
$pdf->WriteHTML("<h1>QUERY 4</h1><br>$htmlTable");
$pdf->Output();
?>
