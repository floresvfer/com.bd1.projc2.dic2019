<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('html_table.php');
require_once '../controlador.php';
require_once '../lib/Servidor_Base_Datos.php';
require_once '../lib/toJSON.php';
require_once '../utils/utils.php';
require('../lib/request.php');

header('Content-Type: text/html');
$htmlTable='<table>
<th>
<td></td>
<td>Country</td>
<td>Population</td>
<td>Central America Population</td>
</th>';

$query = "
select c.name, population, centralamerica.total centralamericapopulation
from country c
         join (select sum(population) total
               from country c
                        join region r on c.region = r.region
               where r.region = 10) centralamerica
where population > centralamerica.total
order by population desc;
";

$request = new request($query);
$array = $request->arrayResponse();

for($i = 0; $i < sizeof($array); $i++){
$htmlTable.= '<tr>';
$htmlTable.= '<td>'.($i+1).'</td>';
$htmlTable.= '<td>'.$array[$i]['name'].'</td>';
$htmlTable.= '<td>'.$array[$i]['population'].'</td>';
$htmlTable.= '<td>'.$array[$i]['centralamericapopulation'].'</td>';
$htmlTable.= '</tr>';
}

$htmlTable.= '</table>';

$pdf=new PDF_HTML_Table();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);
$pdf->WriteHTML("<h1>QUERY 7</h1><br>$htmlTable");
$pdf->Output();
?>
