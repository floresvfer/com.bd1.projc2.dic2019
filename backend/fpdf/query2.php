<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('html_table.php');
require_once '../controlador.php';
require_once '../lib/Servidor_Base_Datos.php';
require_once '../lib/toJSON.php';
require_once '../utils/utils.php';
require('../lib/request.php');

header('Content-Type: text/html');
$htmlTable='<table>
<th>
<td></td>
<td>Senior</td>
<td>Junior</td>
<td>Research Area</td>
</th>';

$query = "
select p.name senior, p2.name junior, ra.description researcharea
from researcharea ra
        left join professional p on ra.boss = p.professional
        join professional_researcharea pr on ra.researcharea = pr.researcharea
        join professional p2 on pr.professional = p2.professional;
;
";

$request = new request($query);
$array = $request->arrayResponse();

for($i = 0; $i < sizeof($array); $i++){
$htmlTable.= '<tr>';
$htmlTable.= '<td>'.($i+1).'</td>';
$htmlTable.= '<td>'.$array[$i]['senior'].'</td>';
$htmlTable.= '<td>'.$array[$i]['junior'].'</td>';
$htmlTable.= '<td>'.$array[$i]['researcharea'].'</td>';
$htmlTable.= '</tr>';
}

$htmlTable.= '</table>';

$pdf=new PDF_HTML_Table();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);
$pdf->WriteHTML("<h1>QUERY 2</h1><br>$htmlTable");
$pdf->Output();
?>
