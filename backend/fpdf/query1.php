<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('html_table.php');
require_once '../controlador.php';
require_once '../lib/Servidor_Base_Datos.php';
require_once '../lib/toJSON.php';
require_once '../utils/utils.php';
require('../lib/request.php');

header('Content-Type: text/html');
$htmlTable='<table>
<th>
<td></td>
<td>Country</td>
<td>Continent</td>
<td>Questions</td>
</th>';

$query = "
select country.name country, continents.continent, count(e.question) questions
from country
         join (select region.region                                                             regionid,
                      region.name                                                               regionname,
                      case when continent.name is null then region.name else continent.name end continent
               from region
                        left join (select *
                                   from region
                                   where regionfather is null) continent
                                  on region.regionfather = continent.region) continents
              on country.region = continents.regionid
         left join evaluation e on country.country = e.country
group by country.name, continents.continent
order by continent
;
";

$request = new request($query);
$array = $request->arrayResponse();

for($i = 0; $i < sizeof($array); $i++){
$htmlTable.= '<tr>';
$htmlTable.= '<td>'.($i+1).'</td>';
$htmlTable.= '<td>'.$array[$i]['country'].'</td>';
$htmlTable.= '<td>'.$array[$i]['continent'].'</td>';
$htmlTable.= '<td>'.$array[$i]['questions'].'</td>';
$htmlTable.= '</tr>';
}

$htmlTable.= '</table>';

$pdf=new PDF_HTML_Table();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);
$pdf->WriteHTML("<h1>QUERY 1</h1><br>$htmlTable");
$pdf->Output();
?>
