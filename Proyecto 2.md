Fernando Josué Flores Valdez, 20150438

Sistemas de Bases de Datos 1

Vacaciones Diciembre 2019

------

## Proyecto 2

# Normalización

## 1FN

***Datos atómicos***

Lo que se busca con esta forma normal es llevar los datos a nuevas tuplas (donde se requiera) con el fin de generar datos relacionados de forma hómogenea, al visualizar los archivos proporcionados se puede constatar que existen datos que se encuentran relacionados entre si, pero que no se encuentran separados (valores separados por comas en una misma columna).

### Multiples Valores

| INVENTO          | INVENTOR        | PROFESIONAL_ASIGNADO |
| ---------------- | --------------- | -------------------- |
| Turbina de gas   | Curtis, Charles | JAMES CLERK          |
| Turbina de vapor | Curtis, Charles | JAMES CLERK          |

| INVENTO          | INVENTOR | PROFESIONAL_ASIGNADO |
| ---------------- | -------- | -------------------- |
| Turbina de gas   | Curtis   | JAMES CLERK          |
| Turbina de gas   | Charles  | JAMES CLERK          |
| Turbina de vapor | Curtis   | JAMES CLERK          |
| Turbina de vapor | Charles  | JAMES CLERK          |

### Llaves Primarias

Posterior a ello tambien se deben distinguir o asignar llaves primarias segun sean los casos, en la mayoria de datos al ser la mayoria de datos texto, es conveniente asignar un valor numerico como codigo unico para facilitar busquedas, asociaciones y pricipalmente garantizar la integridad de nuestro modelo.

|      | INVENTO          | INVENTOR | PROFESIONAL_ASIGNADO |
| ---- | ---------------- | -------- | -------------------- |
| 5    | Turbina de gas   | Curtis   | JAMES CLERK          |
| 5    | Turbina de gas   | Charles  | JAMES CLERK          |
| 19   | Turbina de vapor | Curtis   | JAMES CLERK          |
| 19   | Turbina de vapor | Charles  | JAMES CLERK          |





## 2FN

***Atributos no clave dependen por commpleto del atributo clave***

Podemos observar que, en todas las tablas, todos los valores de las columnas de una fila, dependen de la clave primaria. A diferencia de cómo se encontraba en el archivo csv brindado, al normalizarlo con la segunda forma normal, podemos acceder a los atributos no clave desde su atributo clave, ya que dependen directamente de él. Ejemplo:

| Invento   | Inventor    | Profesional   | Invento_Pais | Profesional_Pais |
| --------- | ----------- | ------------- | ------------ | ---------------- |
| Aeroplano | Ohain       | FORD ANALYST  | ALEMANIA     | ALEMANIA         |
| Aspirina  | Dresser     | JAMES CLERK   | ALEMANIA     | ALEMANIA         |
| Bacteria  | Leeuwenhoek | BLAKE MANAGER | Holanda      | Holanda          |
| Electrón  | Thompson    | JAMES CLERK   | Reino Unido  | ALEMANIA         |

Normalizado

| pais | nombre      |
| ---- | ----------- |
| 1    | ALEMANIA    |
| 2    | Holanda     |
| 3    | Reino Unido |

| invento | pais | nombre    | año  |
| ------- | ---- | --------- | ---- |
| 1       | 1    | Aeroplano | 1990 |
| 2       | 1    | Aspirina  | 1895 |
| 3       | 2    | Bacteria  | 1958 |
| 4       | 3    | Electrón  | 1945 |

| profesional | nombre        | pais |
| ----------- | ------------- | ---- |
| 1           | FORD ANALYST  | 1    |
| 2           | JAMES CLERK   | 1    |
| 3           | BLAKE MANAGER | 2    |



## 3FN

***Todos los Atributos no clave dependen de manera  no transitiva de la clave primaria***

Al almacenar la información en sus respectivas tablas, conseguimos que las
columnas que no forman parte de la clave primaria dependan únicamente de su llave primaria. Por ejemplo, en la tabla país, tenemos que la capital y el nombre se pueden obtener a través del código de país ya que estos dependen exclusivamente de su llave primaria. Si bien está relacionado con region, sus atributos dependen de su código únicamente.

| pais | nombre         | region | nombreregion    | poblacion | area     |
| ---- | -------------- | ------ | --------------- | --------- | -------- |
| 1    | Estados Unidos | 5      | Norte America   | 566612321 | 56423112 |
| 2    | Guatemala      | 9      | Centro America  | 3564122   | 21568    |
| 3    | Alemania       | 3      | Europa del Este | 49875     | 356998   |

| pais | nombre         | region |      | poblacion | area     |
| ---- | -------------- | ------ | ---- | --------- | -------- |
| 1    | Estados Unidos | 5      |      | 566612321 | 56423112 |
| 2    | Guatemala      | 9      |      | 3564122   | 21568    |
| 3    | Alemania       | 3      |      | 49875     | 356998   |

| region | nombreregion    |
| ------ | --------------- |
| 5      | Norte America   |
| 9      | Centro America  |
| 3      | Europa del Este |



## BOYCE-CODD

Podemos Concluir que nuestro modelo se eencuentra en FNBC ya que no existen llaves candidatas que pudean determinar los atributos, las llaves compoestas que existen son necesarias para el modelo y por ello se garantiza que donde estan estas llaves no existen atributos que puedna ser llaves candidatas.





## 4FN

***Dependencias Multivaloradas***

| Encuesta  | Pregunta  | RespuestasPosibles |
| --------- | --------- | ------------------ |
| Encuesta1 | Pregunta1 | a                  |
| Encuesta1 | Pregunta2 | b                  |
| Encuesta1 | Pregunta2 | a                  |
| Encuesta2 | Pregunta2 | c                  |
| Encuesta2 | Pregunta2 | a                  |
| Encuesta2 | Pregunta1 | c                  |

| Encuesta  | Pregunta  |
| --------- | --------- |
| Encuesta1 | Pregunta1 |
| Encuesta1 | Pregunta2 |
| Encuesta2 | Pregunta1 |
| Encuesta2 | Pregunta2 |

| Pregunta  | Respuesta |
| --------- | --------- |
| Pregunta1 | a         |
| Pregunta2 | b         |
| Pregunta2 | a         |
| Pregunta2 | c         |
| Pregunta1 | c         |





## 5FN

Una relación está en 5FN si y sólo si cada dependencia de reunión está implicada en las llaves candidatas y debe satisfacer que la reunión de sus proyecciones sea igual a la relación original.

Un Claro Ejemplo es en Los inventos, donde se asigna un inventor y un profesional, para ello nuestra tabla principal es invento y las tablas a las que se asocia es invento_inventor e invento_profesional, para evitar dejar una de las dos llaves foraneas en una tabla de relacion (invento_inventor o invento_profesional)

| Invento   | Inventor    | Profesional   | Invento_Pais | Profesional_Pais |
| --------- | ----------- | ------------- | ------------ | ---------------- |
| Aeroplano | Ohain       | FORD ANALYST  | ALEMANIA     | ALEMANIA         |
| Aspirina  | Dresser     | JAMES CLERK   | ALEMANIA     | ALEMANIA         |
| Bacteria  | Leeuwenhoek | BLAKE MANAGER | Holanda      | Holanda          |
| Electrón  | Thompson    | JAMES CLERK   | Reino Unido  | ALEMANIA         |

| Inventor | Nombre      |
| -------- | ----------- |
| 1        | Ohain       |
| 2        | Dresser     |
| 3        | Leeuwenhoek |
| 4        | Thompson    |

| Profesional | Nombre        |
| ----------- | ------------- |
| 1           | FORD ANALYST  |
| 2           | JAMES CLERK   |
| 3           | BLAKE MANAGER |

| invento | pais | nombre    | año  |
| ------- | ---- | --------- | ---- |
| 1       | 1    | Aeroplano | 1990 |
| 2       | 1    | Aspirina  | 1895 |
| 3       | 2    | Bacteria  | 1958 |
| 4       | 3    | Electrón  | 1945 |

En este punto, pareciera que todo esta bien, sin embargo esto produce tuplas espuria, por lo tanto hay que separar aun mas, por ello se separa en 3 tablas

| Invento | Inventor | profesional |
| ------- | -------- | ----------- |
| 1       | 1        | 1           |
| 2       | 2        | 2           |
| 3       | 3        | 3           |
| 4       | 4        | 2           |

| invento | inventor |
| ------- | -------- |
| 1       | 1        |
| 2       | 2        |
| 3       | 3        |
| 4       | 4        |

| invento | profesional |
| ------- | ----------- |
| 1       | 1           |
| 2       | 2           |
| 3       | 3           |
| 4       | 2           |

