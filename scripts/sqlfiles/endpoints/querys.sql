#getCountrys
select country.*, r.name regionname from country join region r on country.region = r.region;

#deleteCountry
update country set status = 1 where country = 1;

#getProfessionals
select * from professional;

#getInventors
select inventor.*, c.name countryname from inventor join country c on inventor.country = c.country;

#getInvntions
select invention.*, c.name countryname from invention join country c on invention.country = c.country;

#getRegions
select region, name from region;