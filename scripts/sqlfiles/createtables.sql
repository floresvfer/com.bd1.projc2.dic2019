create table professional(
  professional int auto_increment,
  name varchar(80) not null,
  contractdate datetime default current_timestamp not null,
  salary decimal default 0.0 not null,
  comission decimal default 0.0,
  status int default 0 not null,
  constraint professional_pk
        primary key (professional)
);
insert into professional(name, contractdate, salary, comission)
select distinct professionalassignedtoinvention, str_to_date(professionalcontractdate, '%e-%b-%y'), salary, case when commission = '' then 0 else commission end from pi where professionalassignedtoinvention != '';


create table researcharea
(
    researcharea int auto_increment,
    boss int,
    description varchar(50) not null,
    ranking int default 0 not null,
    status int default 0 not null,
    constraint researcharea_pk
            primary key (researcharea),
    constraint researcharea_professional_professional_pk
        foreign key (boss) references professional (professional)
);

insert into researcharea (boss, description, ranking)
select distinct boss.professional, professionalresearcharea, ranking
from pi left join (select distinct p.professional, professionalisheadofarea
from pi join professional p on pi.professionalassignedtoinvention = p.name where professionalisheadofarea != '')
    boss on boss.professionalisheadofarea = professionalresearcharea
where professionalresearcharea != '';

create table professional_researcharea(
    professional int not null,
    researcharea int not null,
    status int default 0 not null,
    constraint professional_researcharea_pk
        primary key (professional, researcharea),
    constraint professional_researcharea_professional_professional_fk
        foreign key (professional) references professional(professional),
    constraint professional_researcharea_researcharea_researcharea_fk
        foreign key (researcharea) references researcharea(researcharea)
);

insert into professional_researcharea(professional, researcharea)
select distinct p.professional, r.researcharea
from pi join researcharea r on professionalresearcharea = r.description
        join professional p on professionalassignedtoinvention = p.name
where professionalassignedtoinvention != '';


create table region
(
	region int auto_increment,
	regionfather int,
	name varchar(50) not null,
	status int default 0 not null,
	constraint region_pk
		primary key (region),
	constraint region_continent_continent_fk
		foreign key (regionfather) references region (region)
);

insert into region (regionfather, name)
select distinct null, regionname
from piii where regionfather = '';

insert into region (regionfather, name)
select distinct r.region, regionname
from piii join region r on piii.regionfather = r.name;

create table country
(
	country int auto_increment,
	region int not null,
	name varchar(70) not null,
	capital varchar(80) not null,
	population int default 0 not null,
	area int default 0 not null,
	status int default 0 not null,
	constraint country_pk
		primary key (country),
	constraint country_region_region_fk
		foreign key (region) references region (region)
);

insert into country (region, name, capital, population, area)
select distinct region, inventorscountry, capital, populationofthecountry, areainkm2
from pi join region on region.name = pi.regionofthecountry;
insert into country (region, name, capital, population, area)
values (4, 'Belgica', 'Belgica', 2121223, 42342);

create table cardinaldirection(
    cardinaldirection int auto_increment,
    description varchar(10),
    status int default 0 not null,
    constraint cardinaldirection_pk
        primary key (cardinaldirection)
);

insert into cardinaldirection (description) values ('NORTH');
insert into cardinaldirection (description) values ('SOUTH');
insert into cardinaldirection (description) values ('EAST');
insert into cardinaldirection (description) values ('WEST');


create table borderwith
(
	countryborder int not null,
	cardinaldirection int not null,
	country int not null,
	status int default 0 not null,
	constraint borderwith_cardinaldirection_cardinaldirection_fk
		foreign key (cardinaldirection) references cardinaldirection (cardinaldirection),
	constraint borderwith_country_country_fk
		foreign key (countryborder) references country (country),
	constraint borderwith_country_country_fk_2
		foreign key (country) references country (country),
	constraint borderwith_pk
        primary key (countryborder, cardinaldirection, country)
);

insert into borderwith (countryborder, cardinaldirection, country)
select c2.country, cardinaldirection, c1.country
from (
select distinct inventorscountry, borderwith, 1 cardinaldirection from pi where north != '' union
select distinct inventorscountry, borderwith, 2 cardinaldirection from pi where south != '' union
select distinct inventorscountry, borderwith, 3 cardinaldirection from pi where east != '' union
select distinct inventorscountry, borderwith, 4 cardinaldirection from pi where west != ''
) b join country c1 on b.inventorscountry = c1.name
    join country c2 on b.borderwith = c2.name;

create table inventor(
    inventor int auto_increment,
    country int not null,
    name varchar(80) not null,
    status int default 0 not null,
    constraint inventor_country_country_fk
        foreign key (country) references country (country),
    constraint inventor_pk
        primary key (inventor)
);

insert into inventor (country, name)
select distinct c.country, inventor
from pi join country c on c.name = inventorscountry
where inventor != '';

create table invention(
    invention int auto_increment,
    country int not null,
    yearofinvention int not null,
    name varchar(100),
    status int default 0 not null,
    constraint invention_country_country_fk
        foreign key (country) references country(country),
    constraint invention_pk
        primary key (invention)
);

insert into invention(country, yearofinvention, name)
select distinct c.country, yearofinvention, invention
from pi join country c on c.name = countryoftheinviention
where invention != '';

create table inventor_invention(
    inventor int not null,
    invention int not null,
    status int default 0 not null,
    constraint inventor_invention_inventor_inventor_fk
        foreign key (inventor) references inventor (inventor),
    constraint inventor_invention_invention_invention_fk
        foreign key (invention) references invention (invention),
    constraint inventor_invention_pk
        primary key (inventor, invention)
);

insert into inventor_invention(invention, inventor)
select distinct  ion.invention, ior.inventor
from pi left join invention ion on ion.name = pi.invention
        left join inventor ior on ior.name = pi.inventor
where pi.invention != '';

create table professional_invention(
    professional int not null,
    invention int not null,
    status int default 0 not null,
    constraint professional_invention_professional_professional_fk
        foreign key (professional) references professional (professional),
    constraint professional_invention_invention_invention_fk
        foreign key (invention) references invention (invention),
    constraint professional_invention_pk
        primary key (professional, invention)
);

insert into professional_invention(professional, invention)
select distinct  pro.professional, ion.invention
from pi left join invention ion on ion.name = pi.invention
        left join professional pro on pro.name = pi.professionalassignedtoinvention
where pi.invention != '';


create table quiz(
    quiz int auto_increment,
    name varchar(300) not null,
    status int default 0 not null,
    constraint quiz_pk
        primary key (quiz)
);

insert into quiz (name)
select distinct quizname from pii;

create table question(
    question int auto_increment,
    quiz int not null,
    description varchar(500) not null,
    status int default 0 not null,
    constraint question_quiz_quiz_fk
        foreign key (quiz) references quiz(quiz),
    constraint question_pk
        primary key (question)
);

insert into question(quiz, description)
select distinct q.quiz, question
from pii join quiz q on q.name = quizname;


create table answer(
    answer int auto_increment,
    question int not null,
    description varchar(500) not null,
    letter char,
    status int default 0 not null,
    constraint answer_question_question_fk
        foreign key (question) references question (question),
    constraint answer_pk
        primary key (answer)
);

insert into answer(question, description, letter)
select distinct  q.question, trim(substr(possibleanswer, 3, length(possibleanswer))) possibleanswer, substr(possibleanswer, 1, 1) letter
from pii join question q on q.description = pii.question;

create table correct_answer(
    question int not null,
    answer int not null,
    status int default 0 not null,
    constraint correct_answer_question_question_fk
        foreign key (question) references question (question),
    constraint correct_answer_answer_answer_fk
        foreign key (answer) references  answer (answer),
    constraint correct_answer_pk
        primary key (question, answer)
);

insert into correct_answer(question, answer)
select distinct q.question, a.answer
from pii join answer a on a.letter = substr(correctanswer, 1, 1)
         join question q on q.description = pii.question and q.question = a.question;


create table evaluation(
    evaluation int auto_increment,
    country int not null,
    question int not null,
    answer int not null,
    status int default 0 not null,
    constraint evaluation_country_country_fk
        foreign key (country) references country(country),
    constraint evaluation_question_question_fk
        foreign key (question) references question(question),
    constraint evaluation_answer_answer_fk
        foreign key (answer) references answer(answer),
    constraint evaluation_pk
        primary key (evaluation)
);

insert into evaluation(country, question, answer)
select distinct c.country,  q.question, a.answer
from pii join answer a on a.letter = pii.countryresponse
         join question q on q.description = pii.question and q.question = a.question
         join country c on pii.country = c.name;