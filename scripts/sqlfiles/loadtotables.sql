#professional
select distinct professionalisheadofarea, professionalassignedtoinvention, professionalcontractdate, salary, commission from pi where professionalassignedtoinvention != '';

#researcharea
select distinct professionalresearcharea, ranking from pi where professionalresearcharea != '';

#professional_researcharea
select distinct professionalresearcharea, professionalassignedtoinvention from pi where professionalassignedtoinvention != '';

#region
select distinct regionname, regionfather from piii where regionfather != '';

#country
select distinct regionofthecountry, inventorscountry, capital, populationofthecountry, areainkm2 from pi;

#cardinaldirection
#north 1
#south 2
#east 3
#west 4

#borderwith
select distinct inventorscountry, borderwith, 1 from pi where north != '' union
select distinct inventorscountry, borderwith, 2 from pi where south != ''union
select distinct inventorscountry, borderwith, 3 from pi where east != '' union
select distinct inventorscountry, borderwith, 4 from pi where west != '';

#inventor
select distinct inventorscountry, inventor from pi where inventor != '';

#invention
select distinct countryoftheinviention, yearofinvention, invention from pi where invention != '';

#invention_inventor
select distinct invention, inventor from pi where invention != '';

#profesional_invention
select distinct professionalassignedtoinvention, invention from pi where invention != '';

#quiz
select distinct quizname from pii;

#question
select distinct quizname, question from pii;

#answer
select distinct question, trim(substr(possibleanswer, 3, length(possibleanswer))) possibleanswer, substr(possibleanswer, 1, 1) letter from pii;

#correctanswer
select distinct question, trim(substr(correctanswer, 3, length(correctanswer))) correctanswer, substr(correctanswer, 1, 1) letter from pii;

#evaluation
select distinct country, question, countryresponse from pii;