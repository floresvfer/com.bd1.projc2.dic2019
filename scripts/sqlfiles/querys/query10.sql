select name, population
from country
where country not in (select distinct b.country
                      from borderwith b
                      union
                      select distinct b.countryborder
                      from borderwith b)
and area >= (select area from country where lower(name) like '%japon%')
order by population desc;
;