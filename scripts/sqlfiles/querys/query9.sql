select *
from inventor
where lower(name) like '%benz%';

select yearofinvention, count(yearofinvention) from invention group by yearofinvention;

select name, yearofinvention
from invention
where invention.yearofinvention
          in (select ion.yearofinvention year
              from invention ion
                       join inventor_invention ii on ion.invention = ii.invention
                       join inventor ior on ii.inventor = ior.inventor
              where lower(ior.name) like '%benz%')