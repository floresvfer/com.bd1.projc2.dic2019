select
       p.name professional,
       p.salary salary,
       r.description researcharea,
       avgs.avg salaryavg
from professional p
        join professional_researcharea pr on p.professional = pr.professional
        join researcharea r on r.researcharea = pr.researcharea,
        (select
               avg(p.salary) avg,
               r.researcharea
        from professional p
                join professional_researcharea pr on p.professional = pr.professional
                join researcharea r on r.researcharea = pr.researcharea
                group by r.researcharea) avgs
where
      avgs.researcharea = r.researcharea and
      p.salary > avgs.avg
order by r.description;

