select country.name country, continents.continent, count(e.question) questions
from country
         join (select region.region                                                             regionid,
                      region.name                                                               regionname,
                      case when continent.name is null then region.name else continent.name end continent
               from region
                        left join (select *
                                   from region
                                   where regionfather is null) continent
                                  on region.regionfather = continent.region) continents
              on country.region = continents.regionid
         left join evaluation e on country.country = e.country
group by country.name, continents.continent
order by continent
;