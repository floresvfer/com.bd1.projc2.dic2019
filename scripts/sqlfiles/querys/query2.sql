select p.name senior, p2.name junior, ra.description researcharea
from researcharea ra
        left join professional p on ra.boss = p.professional
        join professional_researcharea pr on ra.researcharea = pr.researcharea
        join professional p2 on pr.professional = p2.professional;