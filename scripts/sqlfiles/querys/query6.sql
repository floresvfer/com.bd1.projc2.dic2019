select
       name,
       salary,
       comission,
       (salary + comission) totalsalary,
       (salary) * 0.25 percent
from professional
where comission > 0.25 * salary;