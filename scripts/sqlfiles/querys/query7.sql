select c.name, population, centralamerica.total centralamericapopulation
from country c
         join (select sum(population) total
               from country c
                        join region r on c.region = r.region
               where r.region = 10) centralamerica
where population > centralamerica.total
order by population desc;


