select *
from (select distinct name, area, count(b.countryborder) border
      from country c
               join borderwith b on c.country = b.country
      group by name, area) cs
where cs.border > 7
order by area desc;